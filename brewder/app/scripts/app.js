'use strict';

/**
 * @ngdoc overview
 * @name brewderApp
 * @description
 * # brewderApp
 *
 * Main module of the application.
 */
angular
  .module('brewderApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch'
  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl',
        controllerAs: 'main'
      })
      .when('/demands', {
        templateUrl: 'views/demands.html',
        controller: 'DemandsCtrl',
        controllerAs: 'demands'
      })
      .when('/offers', {
        templateUrl: 'views/offers.html',
        controller: 'OffersCtrl',
        controllerAs: 'offers'
      })
      .when('/location', {
        templateUrl: 'views/location.html',
        controller: 'LocationCtrl',
        controllerAs: 'location'
      })
      .when('/login', {
        controller: 'LoginController',
		    templateUrl: 'views/login.view.html',
        controllerAs: 'vm'
      })
	    .when('/home', {
        controller: 'HomeController',
        templateUrl: 'views/home.view.html',
        controllerAs: 'vm'
      })
      .when('/register', {
        controller: 'RegisterController',
        templateUrl: 'views/register.view.html',
        controllerAs: 'vm'
      })
      .when('/contract/:action', {
        templateUrl: 'views/contract.html',
        controller: 'ContractCtrl',
        controllerAs: 'contract'
      })
      .when('/search', {
        templateUrl: 'views/search.html',
        controller: 'SearchCtrl',
        controllerAs: 'search'
      })
      .when('/interaction/:type', {
        templateUrl: 'views/interaction.html',
        controller: 'InteractionCtrl',
        controllerAs: 'interaction'
      })
      .when('/interaction/:type/:city', {
        templateUrl: 'views/interaction.html',
        controller: 'InteractionCtrl',
        controllerAs: 'interaction'
      })
      .when('/interaction/:type/:city/:datemax', {
        templateUrl: 'views/interaction.html',
        controller: 'InteractionCtrl',
        controllerAs: 'interaction'
      })
      .when('/addentry/:tpe', {
        templateUrl: 'views/addentry.html',
        controller: 'AddentryCtrl',
        controllerAs: 'addentry'
      })
      .otherwise({
        redirectTo: '/'
      });
  });
