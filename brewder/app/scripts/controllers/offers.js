'use strict';

/**
 * @ngdoc function
 * @name brewderApp.controller:OffersCtrl
 * @description
 * # OffersCtrl
 * Controller of the brewderApp
 */
angular.module('brewderApp')
    .controller('OffersCtrl', function breweryController($scope, $routeParams, $http) {
      const data = { sqlquery: "g.V().as('where').inE('offer').outV().has('type','Service').as('what').inE('offers').outV().as('date','capacity').inE('offers').outV().as('brew').select('where','what','date','capacity','brew').by('label').by('label').by('label').by('capacity').by('label').dedup()" };
      $http.post("http://localhost:8080/sqlquery", data).then(function (response) {
      $scope.res = response.data;
    });
    var locationdata = {sqlquery: "g.V().has('type', 'Location').label().dedup()"};
    $http.post("http://localhost:8080/sqlquery", locationdata).then(function (locationresponse) {
      locationresponse.data.sort();
      $scope.locationres = locationresponse.data;
    });
    var servicedata = {sqlquery: "g.V().has('type', 'Service').label().dedup()"};
    $http.post("http://localhost:8080/sqlquery", servicedata).then(function (serviceresponse) {
      serviceresponse.data.sort();
      $scope.serviceres = serviceresponse.data;
    });
    $scope.filtering = function(rval) {
      return (($scope.selectedServiceItem == undefined || rval.what === $scope.selectedServiceItem) && ($scope.selectedLocationItem == undefined || rval.where === $scope.selectedLocationItem));
    }
  });
