﻿'use strict';
angular.module('brewderApp')
  .controller('RegisterController', function RegisterController(UserService, $location, $rootScope, FlashService) {
        var vm = this;
        function register() {
            vm.dataLoading = true;
            UserService.Create(vm.user)
			.then(function (response) {
				if (response.success) {
					FlashService.Success('Registration successful', true);
					$location.path('/login');
				} else {
					FlashService.Error(response.message);
					vm.dataLoading = false;
				}
			});
        }
		vm.register = register;
});
