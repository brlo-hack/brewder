'use strict';

/**
 * @ngdoc function
 * @name brewderApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the brewderApp
 */
angular.module('brewderApp')
  .controller('AboutCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
