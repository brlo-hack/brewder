'use strict';

/**
 * @ngdoc function
 * @name brewderApp.controller:LocationCtrl
 * @description
 * # LocationCtrl
 * Controller of the brewderApp
 */
angular.module('brewderApp')
  .controller('LocationCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
