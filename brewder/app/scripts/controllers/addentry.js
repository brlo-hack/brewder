'use strict';

/**
 * @ngdoc function
 * @name brewderApp.controller:AddentryCtrl
 * @description
 * # AddentryCtrl
 * Controller of the brewderApp
 */
angular.module('brewderApp')
  .controller('AddentryCtrl', function breweryController($scope, $routeParams, $http) {
    $scope.tpe = $routeParams.tpe;
    //var data = { sqlquery: "g.V().as('where').outE('demand').inV().has('type','Service').as('what').outE('seeks').inV().as('date','capacity').outE('seeks').inV().as('brew').select('where','what','date','capacity','brew').by('label').by('label').by('label').by('capacity').by('label').dedup()" };
    //  $http.post("http://localhost:8080/sqlquery", data).then(function (response) {
    //  $scope.res = response.data;

    $scope.submit = function (){
      var locationcheck = { sqlquery :"g.V().has('type','Location').has('label','"+$scope.location+"').Count()" };
      $http.post("http://localhost:8080/sqlquery", locationcheck).then(function (locationcheckresponse) {
        if (locationcheckresponse.data[0] == 0) {
          var locationinsert = {sqlquery: "g.addV(label,'"+$scope.location+"','type','Location')"};
          $http.post("http://localhost:8080/sqlquery", locationinsert);
        }
      });
      var servicecheck = { sqlquery :"g.V().has('label','"+$scope.location+"').inE('offer').outV().has('type','Service').has('label','"+$scope.service+"').Count()" };
      $http.post("http://localhost:8080/sqlquery", servicecheck).then(function (servicecheckresponse) {
        if (servicecheckresponse.data[0] == 0) {
          var vertexdata = { sqlquery: "g.addV(label,'"+$scope.service+"','type','Service')"};
          var vertexid;
          $http.post("http://localhost:8080/sqlquery", vertexdata).then(function (vertexresponse) {
            $scope.vertex = vertexresponse.data[0];
            var vertexid = vertexresponse.data[0].id;
          }).then($scope.test = function() {
            var firstEdgeData = { sqlquery: " g.V().has('label','"+$scope.location+"').addE('demand').to(g.V('"+$scope.vertex.id+"'))"};
            $http.post("http://localhost:8080/sqlquery", firstEdgeData);
            var secondEdgeData = { sqlquery: "g.V('"+$scope.vertex.id+"').addE('offer').to(g.V().has('label','"+$scope.location+"'))"};
            $http.post("http://localhost:8080/sqlquery", secondEdgeData);
          })
        }
      }).then($scope.insert3 = function(){

      var vertex2data = { sqlquery: "g.addV('label','"+$scope.date+"','capacity',"+$scope.capacity+")"};
      var vertex2id;
	    $http.post("http://localhost:8080/sqlquery", vertex2data).then($scope.test3 = function (vertex2response) {
	      $scope.vertex2 = vertex2response.data[0];
        var vertex2id = vertex2response.data[0].id;
	    }).then($scope.test2 = function() {
		    var firstEdgeData = { sqlquery: " g.V().has('label','BitterBrew').addE('offers').to(g.V('"+$scope.vertex2.id+"'))"};
        $http.post("http://localhost:8080/sqlquery", firstEdgeData);
        var secondEdgeData = { sqlquery: "g.V('"+$scope.vertex2.id+"').addE('offers').to(g.V().has('label','"+$scope.location+"').inE('offer').outV().has('label','"+$scope.service+"'))"};
        $http.post("http://localhost:8080/sqlquery", secondEdgeData);
        location.path( "/#!/offers" );
      })
    });
	//nv = g.addV(label,'2017-11-01','capacity',1000)
//brewery  = g.V().has('label','BRLO').addE('seeks').to(nv)
//nv.addE('seeks').to(g.V().has('label','Berlin').outE('demand').inV().has('label','Botteling')

    }

    });
