'use strict';

/**
 * @ngdoc function
 * @name brewderApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the brewderApp
 */
angular.module('brewderApp')
  .controller('MainCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
