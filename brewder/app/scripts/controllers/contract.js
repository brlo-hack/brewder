'use strict';

/**
 * @ngdoc function
 * @name brewderApp.controller:ContractCtrl
 * @description
 * # ContractCtrl
 * Controller of the brewderApp
 */
angular.module('brewderApp')
  .controller('ContractCtrl', function () {
    var $actionparam = "";
    if ($routeParams.action != undefined) {
        $cityparam = ".has('label', '" + $routeParams.city + "')";
    }

    const data = { sqlquery: "g.V()"+$cityparam+".outE('demand').inV() .has('type','Service').as('what').outE('seeks').inV().as('date','capacity').outE('seeks').inV().as('brew').select('what','date','capacity','brew').by('label').by('label').by('capacity').by('label').dedup()"};
      $http.post("http://localhost:8080/sqlquery", data).then(function (response) {
        $scope.res = response.data;
        {{ response.data }}
      }
    );
  });
