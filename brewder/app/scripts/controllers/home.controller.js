﻿'use strict';
angular
	.module('brewderApp')
	.controller('HomeController', function HomeController(UserService, $rootScope) {
	var vm = this;
	vm.user = null;
	
	function initController() {
		loadCurrentUser();
	}
	function loadCurrentUser() {
		UserService.GetByUsername($rootScope.globals.currentUser.username)
			.then(function (user) {
				vm.user = user;
			});
	}
	initController();
});