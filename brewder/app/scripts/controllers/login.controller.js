﻿'use strict';
angular.module('brewderApp')
  .controller('LoginController', function LoginController($location, AuthenticationService, FlashService) {
        var vm = this;
		
		function login() {
            vm.dataLoading = true;
            AuthenticationService.Login(vm.username, vm.password, function (response) {
                if (response.success) {
                    AuthenticationService.SetCredentials(vm.username, vm.password);
                    $location.path('/home');
                } else {
                    FlashService.Error(response.message);
                    vm.dataLoading = false;
                }
            });
        }
        function initController() {
            // reset login status
            AuthenticationService.ClearCredentials();
        }
		initController();
		vm.login = login;
});