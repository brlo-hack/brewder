'use strict';

/**
 * @ngdoc function
 * @name brewderApp.controller:InteractionCtrl
 * @description
 * # InteractionCtrl
 * Controller of the brewderApp
 */
angular.module('brewderApp')
    .controller('InteractionCtrl', function breweryController($scope, $routeParams, $http) {
        var $cityparam = "";
        if ($routeParams.city != undefined) {
            $cityparam = ".has('label','" + $routeParams.city + "')";
        }

        switch ($routeParams.type) {
            case "demands":
                const demanddata = { sqlquery: "g.V()" + $cityparam + ".outE('demand').inV().has('type','Service').as('what').outE('seeks').inV().as('date','capacity').outE('seeks').inV().as('brew').select('what','date','capacity','brew').by('label').by('label').by('capacity').by('label').dedup()" };
                $http.post("http://localhost:8080/sqlquery", demanddata).then(function (response) {
                    $scope.res = response.data;
                    { { response.data } }
                });
                break;
            case "offers":
                const offerdata = { sqlquery: "g.V()" + $cityparam+".inE('offer').outV().has('type','Service').as('what').inE('offers').outV().as('date','capacity').inE('offers').outV().as('brew').select('what','date','capacity','brew').by('label').by('label').by('capacity').by('label').dedup()" };
                $http.post("http://localhost:8080/sqlquery", offerdata).then(function (response) {
                    $scope.res = response.data;
                    { { response.data } }
                });
                break;
            case "Contract":
                const contractdata = { sqlquery: "g.V()" + $cityparam + ".outE('contract').inV().has('type','Service').as('what').outE('seeks').inV().as('date','capacity').outE('seeks').inV().as('brew').select('what','date','capacity','brew').by('label').by('label').by('capacity').by('label').dedup()" };
                $http.post("http://localhost:8080/sqlquery", contractdata).then(function (response) {
                    $scope.res = response.data;
                    { { response.data } }
                });
                break;
            default:
                //do some error handling
                break;
        }

    }
    );
