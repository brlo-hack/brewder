'use strict';

/**
 * @ngdoc function
 * @name brewderApp.controller:SearchCtrl
 * @description
 * # SearchCtrl
 * Controller of the brewderApp
 */
angular.module('brewderApp')
  .controller('SearchCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
