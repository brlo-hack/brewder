'use strict';

describe('Controller: DemandsCtrl', function () {

  // load the controller's module
  beforeEach(module('brewderApp'));

  var DemandsCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    DemandsCtrl = $controller('DemandsCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(DemandsCtrl.awesomeThings.length).toBe(3);
  });
});
