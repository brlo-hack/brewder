'use strict';

describe('Controller: AddentryCtrl', function () {

  // load the controller's module
  beforeEach(module('brewderApp'));

  var AddentryCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    AddentryCtrl = $controller('AddentryCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(AddentryCtrl.awesomeThings.length).toBe(3);
  });
});
